--1.3
-- text / notext
-- lock wird gespeichert
----------------------------------------------------------------------------------------------
-- Hier wird die Hauptvariable "NoOverheal" definiert										--
----------------------------------------------------------------------------------------------
NoOverheal=
{
	TimeLeft = 0;																				-- Variable zur Update/Zeitmessung
	Time = 0;																					-- Variable Interne Uhr
}
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- In der CareerBuffTable sind alle Carrers aufgef�hrt. 1= heiler.							--
-- Eigenlich brauchen wir alle Lines mit 0 nicht, aber so ist h�bscher ;)					--
----------------------------------------------------------------------------------------------
NoOverheal.CareerBuffTable=
		{
			[ 1] = 0	;	-- Eisenbrecher;
			[ 2] = 0	;	-- Slayer;
			[ 3] = 1	;	-- Runenpriester;
			[ 4] = 0	;	-- Maschinist;
			[ 9] = 0	;	-- HexenJaeger;
			[10] = 0	;	-- RitterSonnenorden;
			[11] = 0	;	-- FeuerZauberer;
			[12] = 1	;	-- SigmarPriester;
			[17] = 0	;	-- Schwertmeister;
			[18] = 0	;	-- SchattenKrieger;
			[19] = 0	;	-- WeisserLoewe;
			[20] = 1	;	-- ErzMagier;
   
			[23] = 1	;	-- Khaine;
			[ 6] = 0	;	-- Spalta;
			[21] = 0	;	-- SchwarzerGardist;
			[24] = 0	;	-- Zauberer;
			[22] = 0	;	-- HexenKrieger;
			[ 7] = 1	;	-- Schamane;
			[ 8] = 0	;	-- Squigtreiba;
			[ 5] = 0	;	-- SchwarzOrk;
			[16] = 0	;	-- Magus;
			[15] = 1	;	-- Zelot;
			[14] = 0	;	-- ChaosBarbar;
			[13] = 0	;	-- Auserkorener;
		};
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- Die Funktion NoOverheal.OnInitialize wird zuerst ausgef�hrt.								--
-- Hier	werden Grundlegende Dinge f�rs Addon eingestellt. Variablen definiert etc. 			--
----------------------------------------------------------------------------------------------
function NoOverheal.OnInitialize()
	if not NoOverheal.save then																	-- Save Variablen noch nicht angelegt ?
		NoOverheal.save = {}																	-- Variable Dimensionieren
		NoOverheal.save.MaxHealth = 99															-- Standardm��ig wird bei �ber 99% Leben die Heilung abgebrochen
		NoOverheal.save.active = true															-- Addon ist an
		NoOverheal.save.chattext = true															-- Addon ist an
		NoOverheal.save.locked = false															-- Fenster ist verschiebbar.
	end
	CreateWindow("NoOverheal_Window", true);													-- Optionfenster erstellen
	LayoutEditor.RegisterWindow("NoOverheal_Window",L"NoOverheal",L"main",true,true,true,nil );	-- Fenster f�r den Layouteditor registrieren.
	WindowSetFontAlpha ("NoOverheal_Window", 0.5);												-- Schrift transparenz 50%
	WindowSetFontAlpha ("NoOverheal_WindowText", 1.0);											-- Transparenz des Textes in NoOverheal_Window
	LabelSetText ("NoOverheal_WindowText",NoOverheal.ADDONNAME);								-- Text auf "NoOverheal" festlegen
	LabelSetTextColor("NoOverheal_WindowText", 255, 0 , 20 );									-- Textfarbe in NoOverheal_Window
	LabelSetText("NoOverheal_WindowNameText",NoOverheal.NOTARGET);								-- Text auf "kein Guard!" festlegen
	LabelSetTextColor("NoOverheal_WindowNameText", 140 , 140 , 140 );							-- Textfarbe in NoOverheal_Window
	RegisterEventHandler (SystemData.Events.PLAYER_BEGIN_CAST,"NoOverheal.OnPlayerBeginCast" )	-- EventHandler Aktions beginn
	RegisterEventHandler (SystemData.Events.PLAYER_END_CAST,"NoOverheal.OnPlayerEndCast")		-- EventHandler Aktions ende
	RegisterEventHandler (SystemData.Events.SPELL_CAST_CANCEL,"NoOverheal.OnCancelSpell")		-- EventHandler Aktion abgebrochen
	NoOverheal.SetWindowLockState()																-- Addonfenster Fixieren oder Verschiebbar machen.
	NoOverheal.SlashHandlerInit()
	EA_ChatWindow.Print(NoOverheal.GREETINGS);													-- Begr��ungstext wird hier in den Chat geschrieben.
end   
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- Die Funktion NoOverheal.OnShutdown wird beim Spiel beenden oder ausloggen ausgef�hrt. Wir--
-- unregistrieren hier nur die Eventhandler. Und das auch nur weil's sch�n ordentlich ist :)--
----------------------------------------------------------------------------------------------
function NoOverheal.OnShutdown()
	UnregisterEventHandler (SystemData.Events.PLAYER_BEGIN_CAST ,"NoOverheal.OnPlayerBeginCast")-- EventHandler beenden
	UnregisterEventHandler (SystemData.Events.PLAYER_END_CAST,"NoOverheal.OnPlayerEndCast")		-- EventHandler beenden
	UnregisterEventHandler (SystemData.Events.SPELL_CAST_CANCEL,"NoOverheal.OnCancelSpell")		-- EventHandler beenden
end   
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- Die Funktion NoOverheal.CreateTooltip erstellt einen Tooltip das am Fenster "window" 	--
-- verankert ist, und den Text "text" enth�lt												--
----------------------------------------------------------------------------------------------
function NoOverheal.CreateTooltip(window, text)
   Tooltips.CreateTextOnlyTooltip( window, text )												-- Nur-Text Tooltip erstellen.
   Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_TOP)											-- Tooltip verankern
end
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- Hier ist das Standard Update eines Jeden Addons. In unserem Fall werden hier die 		--
-- Lebensdaten aller Spieler in der Gruppe gesammelt, und im selben Zug ausgewertet.		--
----------------------------------------------------------------------------------------------
function NoOverheal.OnUpdate(elapsedTime)
	NoOverheal.Time=NoOverheal.Time+elapsedTime;												-- Interne Uhr mitlaufen lassen
	-- ADDONBREMSE -------------------------------------------------
	NoOverheal.TimeLeft = NoOverheal.TimeLeft - elapsedTime
    if NoOverheal.TimeLeft > 0 then return; end;
	NoOverheal.TimeLeft = 0.1
	-- ADDONBREMSE -------------------------------------------------
	NoOverheal.UpdateHealthArray()
end
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- Mit der linken Maustaste wird hier die aktuelle Aktion abgebrochen, falls das Fenster	--
-- nicht verschiebbar ist.																	--
----------------------------------------------------------------------------------------------
function NoOverheal.OnLButtonUp()
	if not WindowGetMovable( "NoOverheal_Window" ) then											-- Ist das Fenster verschiebbar ?
		CancelSpell()																			-- Heilung abbrechen
	end
end
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- Mit der mittleren Maustaste wird hier das NoOverhealfenster fixiert oder verschiebbar	--
-- gemacht.																					--
----------------------------------------------------------------------------------------------
function NoOverheal.OnMButtonUp()
	NoOverheal.save.locked = not NoOverheal.save.locked											-- Wechseln zwischen verschiebbar oder nicht.
	NoOverheal.SetWindowLockState()																-- Addonfenster Fixieren oder Verschiebbar machen.
end
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- Mit der Rechten Maustaste bestimmen wir ob Overheal an oder aus ist.					 	--
-- 																							--
----------------------------------------------------------------------------------------------
function NoOverheal.OnRButtonUp()
	NoOverheal.save.active = not NoOverheal.save.active											-- an=aus oder aus=an
	if NoOverheal.save.active then																-- Ist das Addon jetzt an ?
		EA_ChatWindow.Print(NoOverheal.ON)														-- "AN" Meldung ausgeben
	else																						-- Ist das Addon jetzt aus ?
		EA_ChatWindow.Print(NoOverheal.OFF)														-- "AUS" Meldung ausgeben
	end
end
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- Sobald der Mauszeiger �ber dem Fenster gezogen wird, erscheint hier ein Tooltip			--
-- Falls der Fenster fixiert ist, wird kein Tooltip angezeigt.								--
----------------------------------------------------------------------------------------------
function NoOverheal.OnMouseOver()
	LabelSetText ("NoOverheal_WindowText",NoOverheal.ADDONNAME);								-- Text auf "NoOverheal" festlegen
	LabelSetTextColor("NoOverheal_WindowText", 200, 10 , 40 );									-- Textfarbe in NoOverheal_Window
	if WindowGetMovable( "NoOverheal_Window" ) then												-- Ist das Fenster verschiebbar ?
		NoOverheal.CreateTooltip("NoOverheal_Window", NoOverheal.TOOLTIPTEXT)					-- ja, dann Tooltip zeigen.
	end
end
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- Tooltip auschalten wenn der Mauszeiger das Fenster wieder verl�sst						--
-- 																							--
----------------------------------------------------------------------------------------------
function NoOverheal.OnMouseOverEnd()
	LabelSetText ("NoOverheal_WindowText",NoOverheal.ADDONNAME);								-- Text auf "NoOverheal" festlegen
	LabelSetTextColor("NoOverheal_WindowText", 255, 0 , 20 );									-- Textfarbe in NoOverheal_Window
end
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- Diese Funktion wird �ber das BEGINN_CAST event ausgel�st und merkt sich den Namen des	--
-- Ziels, falls ein Heilspruch benutzt wurde.												--
----------------------------------------------------------------------------------------------
function NoOverheal.OnPlayerBeginCast(actionId, isChannel, desiredCastTime, averageLatency)
	NoOverheal.curHealName = nil;																-- Neuer Zauberspruch. Name erstmal l�schen
	local abilityData = Player.GetAbilityData (actionId);										-- Daten des Zauberspruchs auslesen
	if abilityData then																			-- Haben wir Daten bekommen ?
		if abilityData.isHealing then															-- Heilt der Zauberspruch ?
			if abilityData.targetType == 2 then													-- Wirkt der Zauberspruch auf das Freundliche Ziel ?
				if desiredCastTime > 0.5 then													-- Zauberdauer l�nger als 0.5 sek ?
					NoOverheal.curHealName = TargetInfo:UnitName("selffriendlytarget")			-- Wir heilen! Namen vom Ziel merken.
					if not NoOverheal.curHealName or NoOverheal.curHealName == L"" then			-- Haben wir ein Friendly Target ?
						NoOverheal.curHealName = GameData.Player.name							-- Name von mir selber merken :)
					end
					LabelSetText("NoOverheal_WindowNameText",L""..NoOverheal.curHealName);		-- Name ins NoOverheal Fenster schreiben
					LabelSetTextColor("NoOverheal_WindowNameText", 0, 255 , 0 );				-- Textfarbe in NoOverheal_Window
				end
			end
		end
	end
end
----------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------
-- Dieses Event wird gefeuert wenn ein Zauberspruch/Aktion beendet ist.						--
-- 																							--
----------------------------------------------------------------------------------------------
function NoOverheal.OnPlayerEndCast()
	NoOverheal.curHealName = nil;																-- Neuer Zauberspruch. Name erstmal l�schen
	LabelSetText("NoOverheal_WindowNameText",NoOverheal.NOTARGET);								-- Text auf "kein Guard!" festlegen
	LabelSetTextColor("NoOverheal_WindowNameText", 140, 140 , 140 );							-- Zauber beendet. Text grau f�rben. 
end
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- Dieses Event wird gefeuert wenn ein Zauberspruch/Aktion abgebrochen wurde.				--
-- Das wird auch ausgel�st, wenn wir per cancelspell() den Spruch abbrechen					--
----------------------------------------------------------------------------------------------
function NoOverheal.OnCancelSpell()
	NoOverheal.curHealName = nil;																-- Neuer Zauberspruch. Name erstmal l�schen
	LabelSetTextColor("NoOverheal_WindowNameText", 255, 255 , 255 );							-- Zauber abgebrochen. Text rot f�rben. 
end
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- Hier ist das Kernst�ck der Addons. An dieser Stelle werden aus Gruppenfenstern etc die	--
-- Lebens% ausgelesen, und verglichen														--
----------------------------------------------------------------------------------------------
function NoOverheal.UpdateHealthArray()
	-------------------------------- Kriegstrupp ---------------------------------------------
	if IsWarBandActive() then																	-- besteht ein Kriegstrupp ?
		NoOverheal.Raidgroups = GetBattlegroupMemberData()
		for groupIndex = 1, 4 do																-- Gruppen 1 bis 4 durchz�hlen
			for memberIndex = 1, 6 do															-- Gruppenmitglieder 1 bis 6 durchz�hlen
				local Player = NoOverheal.Raidgroups[groupIndex].players[memberIndex]			-- Variable etwas �bersichtlicher machen.
				if (Player) then																-- Existiert Gruppenmitglied ?
					local HP = Player.healthPercent												-- Lebens% auslesen
					local Name = Player.name													-- Namen Merken
					NoOverheal.CheckOverheal(Name,HP)											-- Pr�fen ob das mein Heilziel ist.
				end
			end
		end
	else
	------------------------------ Normale 6er Gruppe ----------------------------------------
		local HP = GameData.Player.hitPoints.current*100/GameData.Player.hitPoints.maximum		-- Lebens% von mir selbst
		local Name = GameData.Player.name														-- Ich :)
		NoOverheal.CheckOverheal(Name,HP)														-- Pr�fen ob ich selber das Heilziel bin.
		for MemberIndex=1,5,1 do																-- Schleife von 1 bis 5 in 1.er Schritten
			if GroupWindow.IsMemberValid(MemberIndex) then										-- Existiert Gruppenmitglied ?
				local HP,ap,morale,lvl,petHp,rvrFlag = GetGroupMemberStatusData(MemberIndex)	-- Lebens% und viel sinnloses Zeug auslesen
				local Name = GroupWindow.groupData[MemberIndex].name							-- Name Merken
				NoOverheal.CheckOverheal(Name,HP)												-- Pr�fen ob das mein Heilziel ist.
			end
		end
	end
	------------------------------ Freundliches Ziel ----------------------------------------
	local HP = TargetInfo:UnitHealth("selffriendlytarget")										-- Lebens% vom Ziel auslesen
	local Name =  TargetInfo:UnitName ("selffriendlytarget");									-- Name des Ziels merken.
	NoOverheal.CheckOverheal(Name,HP)															-- Pr�fen ob das mein Heilziel ist.
end
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- Hier wird gepr�ft ob der aktuelle Name meinem heilziel entspricht, und wenn ja;			--
-- schauen wir ob die Lebens% �ber dem Gew�nschten Wert liegen, und brechen dann Heilung ab	--
----------------------------------------------------------------------------------------------
function NoOverheal.CheckOverheal(name,health)
	if not NoOverheal.curHealName then return;end;												-- Wir haben kein HeilZiel
	if not NoOverheal.save.active then return;end;												-- Addon aus ? dann zur�ck
	if NoOverheal.TrimName(NoOverheal.curHealName) == NoOverheal.TrimName(name) then 			-- Ist das unser Heilziel ?
		if health >= NoOverheal.save.MaxHealth then												-- leben �ber= x% ?
			if NoOverheal.save.chattext then													-- Soll Text in den Chat geschrieben werden ?
				EA_ChatWindow.Print(NoOverheal.PREVENT..NoOverheal.curHealName);				-- Chatmitteilung ausgeben.
			end
			CancelSpell()																		-- Heilung abbrechen
		else																					-- Heilziel braucht heilung!
			LabelSetText("NoOverheal_WindowNameText",L""..NoOverheal.curHealName..L" ("..health..L")");	-- Name ins NoOverheal Fenster schreiben
		end
	end
end
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- Hier werden die Namen der Spieler aus dem target und aus der Gruppe in ein einheitliches --
-- Format gebracht. Falls n�tig wird "Archivar^F" zu L"Archivar"							--
----------------------------------------------------------------------------------------------
function NoOverheal.TrimName(SpielerName)
	if (not SpielerName) then return L""; end; 													-- Kein Name ? Dann kein Target !
	if type(SpielerName) == "string" then														-- Name im string Format ?
		SpielerName = towstring(SpielerName)													-- zum WString umwandeln
	end
	local stringName = WStringToString(SpielerName) 											-- Namen in normalen string konvertieren
	if string.len(stringName) < 2 then															-- L�nge des Namens kleiner als 2 Zeichen ?
		return L""																				-- Name zu klein. Kein Target!
	end
	if "^" == string.sub(stringName, string.len(stringName)-1, string.len(stringName)-1) then	-- Sonderzeichen ^ im Namen enthalten ?
		SpielerName = towstring (string.sub(stringName, 1, string.len(stringName)-2))			-- "Archivar^F" zu L"Archivar" machen
	end
	return SpielerName																			-- Bereinigten Namen zur�ckgeben
end
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- Hier wird das Addonfenster entweder Verschiebbar gemacht oder fixiert. Jenachdem was in  --
-- der Variable Save.locked gespeichert ist.												--
----------------------------------------------------------------------------------------------
function NoOverheal.SetWindowLockState()
	if NoOverheal.save.locked then																-- Locked wahr ?
		WindowSetMovable( "NoOverheal_Window", false)											-- Fenster Locken
		EA_ChatWindow.Print(NoOverheal.ADDONNAME..L": "..NoOverheal.LOCKED);					-- Auch in den Chat schreiben.
	else
		WindowSetMovable( "NoOverheal_Window", true)											-- Fenster freigeben
		EA_ChatWindow.Print(NoOverheal.ADDONNAME..L": "..NoOverheal.MOVEABLE);					-- entsprechenden Text ausgeben.
	end
end
----------------------------------------------------------------------------------------------




