----------------------------------------------------------------------------------------------
-- Allgemeine Initalisierung von Variablen, Arrays usw.										--
----------------------------------------------------------------------------------------------
function NoOverheal.SlashHandlerInit()
    NoOverheal.original_OnKeyEnter = EA_ChatWindow.OnKeyEnter									-- Den original H�ndler Speichern
    EA_ChatWindow.OnKeyEnter = NoOverheal.OnKeyEnter											-- Original H�ndler durch eigenen ersetzen.
end
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- EA_ChatWindow.OnKeyEnter Umleitung. Hier kommt das an, was in die Chatbox geschrieben 	--
-- und [ENTER] gedr�ckt wurde. Hier fangen wir alles ab, was nicht gesendet werden soll.	--
----------------------------------------------------------------------------------------------
function NoOverheal.OnKeyEnter(...)
	local ChatText = EA_TextEntryGroupEntryBoxTextInput.Text									-- ChatText auslesen
    local Befehl																				-- Slashbefehle werden hier gespeichert
	local Args																					-- Argumente, falls es welche gibt.
    local Befehl, Args = ChatText:match(L"^/([a-zA-Z0-9]+)[ ]?(.*)")							-- Befehle und Argumente trennen.
	if Befehl == L"noo" then																	-- SlashBefehl /noo gefunden ?
		NoOverheal.SlashBoy(Args)																-- SlashBefehle auswerten
	    EA_TextEntryGroupEntryBoxTextInput.Text = L""											-- Chat L�schen, /noo Commando wird ausgef�hrt.
	end
    NoOverheal.original_OnKeyEnter(...)															-- Original H�ndler ausf�hren
end
----------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------
-- Aurgumente des /noo Slashcommands auswerten											 	--
-- 																							--
----------------------------------------------------------------------------------------------
function NoOverheal.SlashBoy(Args)
	if Args == L"on" or  Args == L"an" then														-- Befehl on
		NoOverheal.save.active = true															-- Flag aktiv setzen
		EA_ChatWindow.Print(NoOverheal.ON)														-- "AN" Meldung ausgeben
	elseif Args == L"off" or  Args == L"aus" then												-- Befehl aus
		NoOverheal.save.active = false															-- Flag passiv setzen
		EA_ChatWindow.Print(NoOverheal.OFF)														-- "AN" Meldung ausgeben
	elseif Args == L"lock" then																	-- Befehl lock
		NoOverheal.save.locked = true															-- Addonfenster fixieren
		NoOverheal.SetWindowLockState()															-- Addonfenster Fixieren oder Verschiebbar machen.
	elseif Args == L"unlock" then																-- Befehl unlock
		NoOverheal.save.locked = false															-- Addonfenster freigeben
		NoOverheal.SetWindowLockState()															-- Addonfenster Fixieren oder Verschiebbar machen.
	elseif Args == L"text" then																	-- Befehl text
		NoOverheal.save.chattext = true															-- Chattext ist an
		EA_ChatWindow.Print(NoOverheal.ADDONNAME..L": "..NoOverheal.TEXT);						-- entsprechenden Text ausgeben.
	elseif Args == L"notext" then																-- Befehl notext
		NoOverheal.save.chattext = false														-- Chattext ist aus
		EA_ChatWindow.Print(NoOverheal.ADDONNAME..L": "..NoOverheal.NOTEXT);					-- entsprechenden Text ausgeben.
	else																						-- keine Kommandos mehr ? Dann Hilfetext
		EA_ChatWindow.Print(NoOverheal.HELPHEADLINE)											-- Hilfetext
		EA_ChatWindow.Print(NoOverheal.HELPON)													-- Hilfetext
		EA_ChatWindow.Print(NoOverheal.HELPOFF)													-- Hilfetext
		EA_ChatWindow.Print(NoOverheal.HELPLOCK)												-- Hilfetext
		EA_ChatWindow.Print(NoOverheal.HELPUNLOCK)												-- Hilfetext
		EA_ChatWindow.Print(NoOverheal.HELPTEXT)												-- Hilfetext
		EA_ChatWindow.Print(NoOverheal.HELPNOTEXT)												-- Hilfetext
		EA_ChatWindow.Print(L"(lock="..towstring (booltostring (NoOverheal.save.locked))..L")")
	end
end
----------------------------------------------------------------------------------------------
