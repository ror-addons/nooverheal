<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

	<UiMod name="NoOverheal" version="1.0.4" date="2016-11-18" >
		<Author name="Archivar" email="Archivar@ZwergenTeam.de" />
		<Description text="no overheal" />
		<Dependencies>
			<Dependency name="EA_ChatWindow" />
		</Dependencies>
		<SavedVariables>
			<SavedVariable name="NoOverheal.save" />
		</SavedVariables>
		<Files>
			<File name="NoOverheal.xml" />
		</Files>
		<OnInitialize>
			<CallFunction name="NoOverheal.OnInitialize" />
		</OnInitialize>
		<OnShutdown>
			<CallFunction name="NoOverheal.OnShutdown" />
		</OnShutdown>
		<OnUpdate>
			<CallFunction name="NoOverheal.OnUpdate" />
		</OnUpdate>

	</UiMod>
</ModuleFile>

