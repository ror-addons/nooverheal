-------------------------------------------------------------------------------
-- German localization
-------------------------------------------------------------------------------
if ( SystemData.Settings.Language.active == SystemData.Settings.Language.GERMAN ) then

	NoOverheal.ADDONNAME		= L"NoOverheal"
	NoOverheal.NOTARGET			= L"kein Ziel."
	NoOverheal.TOOLTIPTEXT		= L"Linke Maustaste =\n Zauber/Aktion abbrechen.\n nur bei fixiertem Fenster\nRechte Maustaste =\n �berheilung an/aus.\nMittlere Maustaste =\n Fenster fixieren & l�sen.\n Kein Tooltip bei fixiertem Fenster!"
	NoOverheal.GREETINGS		= L"noOverheal: Addon geladen. Slash: /noo"
	NoOverheal.MOVEABLE			= L"Fenster ist verschiebbar."
	NoOverheal.LOCKED			= L"Fenster ist fixiert."
	NoOverheal.PREVENT			= L"Verhindere �berheilung auf "
	NoOverheal.ON				= L"NoOverheal an. Du kannst nicht �berheilen."
	NoOverheal.OFF				= L"NoOverheal aus. Du kannst �berheilen."
	NoOverheal.TEXT				= L"Chatbenachrichtigung bei Zauberabbruch ein"
	NoOverheal.NOTEXT			= L"Keine Chatnachricht bei Zauberabbruch"
	NoOverheal.HELPHEADLINE		= L"NoOverheal Slashbefehle:"
	NoOverheal.HELPON			= L"/noo on - Schaltet noOverheal ein"
	NoOverheal.HELPOFF			= L"/noo off - Schaltet noOverheal aus"
	NoOverheal.HELPLOCK			= L"/noo lock - Fixiert das Addon Fenster"
	NoOverheal.HELPUNLOCK		= L"/noo unlock - Addon Fenster frei verschiebbar"
	NoOverheal.HELPTEXT			= L"/noo text - Chathinweis bei Zauberunterbrechung"
	NoOverheal.HELPNOTEXT		= L"/noo notext - Kein Chattext ausgeben"

end


