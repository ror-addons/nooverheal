-------------------------------------------------------------------------------
-- english localization
-------------------------------------------------------------------------------
-- english is the mainlanguage and will be set first. localization.en.lua has to be loaded first!
--if ( CoreLanguageDirectories[SystemData.Settings.Language.active] == "english" ) then

	NoOverheal.ADDONNAME		= L"NoOverheal"
	NoOverheal.NOTARGET			= L"no target."
	NoOverheal.TOOLTIPTEXT		= L"left mousebutton =\n cancel cast/ability.\n only if window is locked\nright mousebutton =\n Overheal on/off.\nmiddle mousebutton =\n lock/unlock window.\n no tooltip while window is locked"
	NoOverheal.GREETINGS		= L"noOverheal: addon loaded. Slash: /noo"
	NoOverheal.MOVEABLE			= L"window is moveable."
	NoOverheal.LOCKED			= L"window is locked."
	NoOverheal.PREVENT			= L"prevented overheal on "
	NoOverheal.ON				= L"NoOverheal on. You can't overheal now."
	NoOverheal.OFF				= L"NoOverheal off. You can overheal now."
	NoOverheal.TEXT				= L"chatnote on cancelspell"
	NoOverheal.NOTEXT			= L"no cancelspell notification"
	NoOverheal.HELPHEADLINE		= L"NoOverheal slashcommands:"
	NoOverheal.HELPON			= L"/noo on - switch noOverheal on"
	NoOverheal.HELPOFF			= L"/noo off - switch noOverheal off"
	NoOverheal.HELPLOCK			= L"/noo lock - lock addon window"
	NoOverheal.HELPUNLOCK		= L"/noo unlock - unlock addon window"
	NoOverheal.HELPTEXT			= L"/noo text - chatnote on cancelspell"
	NoOverheal.HELPNOTEXT		= L"/noo notext - no cancelspell notification"

--end